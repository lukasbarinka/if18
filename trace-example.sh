#!/bin/bash

TMP=$(mktemp -d) || { echo "$0: Can't create temp dir"; exit 2; } >&2
trap 'rm -rf "$TMP"; exit' EXIT
PIPE=$TMP/debug

[ -p "$PIPE" ] || mkfifo "$PIPE" || { echo "$0: Can't create pipe '$PIPE'"; exit 2; } >&2
gnome-terminal -- cat "$PIPE" - &
./f.sh 3>"$PIPE"
