#!/bin/bash
source ./trace.sh

f() {
	declare -p FUNCNAME BASH_SOURCE BASH_LINENO
	echo ----
	watch Y
	((X++,Y*=2))
	f
	date
}
a() {
	f
	pwd
}
watch X
X=0 Y=1
#declare -tf a f
FUNCNEST=5
a
echo $X $Y
