#!/bin/bash

mapfile -t -O1 <"${BASH_SOURCE[@]: -1}"
declare -A watched=()

trace() {
	local N l stack vars v
	case ${FUNCNAME[1]} in
		watch) return;;
	esac
	N=$(caller)
	N=${N%% *}
	MAPFILE[$N]=$'\e[07;31m'"${MAPFILE[$N]}"$'\e[0m'
	clear
	printf -- '%s\n' "${MAPFILE[@]}"
	MAPFILE[$N]=${MAPFILE[$N]/$'\e[07;31m'/$'\e[01;31m'}
	printf -v stack -- '\u2190 %s ' "${FUNCNAME[@]:1}"
	for v in "${!watched[@]}"; do vars+="$v[${!v}] "; done
	stack=${stack#$'\u2190 '}
	stack=$'\e]0;'"$stack"$'\a'$'\e[1;34m'"$stack{ $vars}"$'\e[0m'
	printf '\n%s' "$stack"
	read -sn1 -t1
	sleep 0
} >&3

watch() {
	local i
	declare -g "$@"
	for i; do watched["$i"]=; done
}

read -p "--- Press any key or wait for 5 seconds to start  ---" -sn1 -t5 2>&3

shopt -s extdebug
trap trace DEBUG
#set -o functrace # default by extdebug
